#define MODEL_IDENTIFIER pandapower
#include "Python.h"
#include "libxml/parser.h"
#include "fmi2Functions.h"
#include <dlfcn.h>

fmi2Real sim_time;
int modifiedState = 1;
PyObject *pandapower = NULL;
PyObject *net = NULL;
PyObject *pp_pf_runpp_3ph = NULL;
PyObject *py_string_runpp_3ph = NULL;
char dataframeOfValueReference[100000][1024];
int indexOfValueReference[100000];
char columnOfValueReference[100000][1024];

void initValueReferenceArrays(char * modelPath) {
  xmlDocPtr modelDescription = xmlParseFile(modelPath);
  if (modelDescription == NULL){
      printf("modelDescription.xml not found.\n");
  }
  xmlNodePtr currentNode = xmlDocGetRootElement(modelDescription);
  if (currentNode == NULL){
      printf("unable to find the root of modelDescription.xml.\n");
  }
  currentNode = currentNode->children->next;
  while (currentNode != NULL && (strcmp(currentNode->name, "ModelVariables") != 0)) {
    currentNode = currentNode->next->next;
  }
  currentNode = currentNode->children->next;
  for (currentNode; currentNode != NULL; currentNode=currentNode->next->next) {
    int valueReference = strtol(xmlGetProp(currentNode, "valueReference"), NULL, 10);
    char *name = xmlGetProp(currentNode, "name");
    char *dataframe = strsep(&name,"/");
    char *varName = strsep(&name,"/");
    char *column = strsep(&name,"/");
    char *tmp = dataframe;
    if (strstr(tmp, "res_") != NULL) {
      tmp = tmp + 4;
    }
    char strippedDataframe[1024];
    strcpy(strippedDataframe, tmp);
    if (strstr(strippedDataframe, "_3ph") != NULL) {
      strippedDataframe[strlen(strippedDataframe) - 4] = '\0';
    }
    PyObject *py_strippedDataframe = Py_BuildValue("s", strippedDataframe);
    PyObject *py_varName = Py_BuildValue("s",varName);
    PyObject *pp_get_element_index = PyObject_GetAttrString(pandapower, "get_element_index");
    PyObject *py_index = PyObject_CallFunctionObjArgs(pp_get_element_index, net, py_strippedDataframe, py_varName, NULL);
    int index = PyLong_AsLong(py_index);
    strcpy(dataframeOfValueReference[valueReference], dataframe);
    indexOfValueReference[valueReference] = index;
    strcpy(columnOfValueReference[valueReference], column);
    Py_DECREF(py_strippedDataframe);
    Py_DECREF(py_varName);
    Py_DECREF(pp_get_element_index);
    Py_DECREF(py_index);
  }
  xmlFreeDoc(modelDescription);
  xmlCleanupParser();
}

void updatePandapower(void){
  if (modifiedState){
    // pp.pf.runpp_3ph.runpp_3ph(net)
    PyObject * result = PyObject_CallMethodObjArgs(pp_pf_runpp_3ph, py_string_runpp_3ph, net, NULL);
    // net.converged
    PyObject *net_converged = PyObject_GetAttrString(net, "converged");
    if (!PyObject_IsTrue(net_converged)) {
        printf("Error while running unbalanced load flow of pandapower network.\n");
        PyErr_Print();    
    }
    modifiedState = 0;
    Py_DECREF(result);
    Py_DECREF(net_converged);
    }
}

// return a PyObject holding the value of the pandapower dataframe df for
// variable v in column c
// varStdName looks like this: df/v/c
PyObject *getValue(int valueReference) {
  updatePandapower();
  // net.dataframe
  PyObject *net_dataframe = PyObject_GetAttrString(net, dataframeOfValueReference[valueReference]);
  // net.dataframe.at
  PyObject *net_dataframe_at = PyObject_GetAttrString(net_dataframe, "at");
  // [index,col]
  PyObject *index_col = Py_BuildValue("i,s", indexOfValueReference[valueReference], columnOfValueReference[valueReference]);
  // net.dataframe.at[index, col]
  PyObject *py_value = PyObject_GetItem(net_dataframe_at, index_col);
  Py_DECREF(net_dataframe);
  Py_DECREF(net_dataframe_at);
  Py_DECREF(index_col);
  return py_value;
}

// set the pandapower dataframe df for variable v in column c to value py_value
// varStdName looks like this: df/v/c
void setValue(int valueReference, PyObject *py_value) {
  // net.dataframe
  PyObject *net_dataframe = PyObject_GetAttrString(net, dataframeOfValueReference[valueReference]);
  // net.dataframe.at
  PyObject *net_dataframe_at = PyObject_GetAttrString(net_dataframe, "at");
  // [index,col]
  PyObject *index_col = Py_BuildValue("i,s", indexOfValueReference[valueReference], columnOfValueReference[valueReference]);
  // net.dataframe.at[index,col] = value
  PyObject_SetItem(net_dataframe_at, index_col, py_value);
  modifiedState = 1;
  Py_DECREF(net_dataframe);
  Py_DECREF(net_dataframe_at);
  Py_DECREF(index_col);
}

/****************************************************
Common Functions
****************************************************/

const char* fmi2GetTypesPlatform(void){
    return "default";
}

const char* fmi2GetVersion(void){
    return "2.0";
}

fmi2Status  fmi2SetDebugLogging(fmi2Component c, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[]){
    // NOT IMPLEMENTED
    return fmi2OK;
}

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID, fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions, fmi2Boolean visible, fmi2Boolean loggingOn){
    Py_Initialize();
    // import pandapower
    pandapower = PyImport_ImportModule("pandapower");
    char network_path[1024];
    strcpy(network_path, fmuResourceLocation);
    memmove(network_path, network_path + 7, strlen(network_path));
    strcat(network_path, "/network.json");
    // py string "path_to_network/network.json"
    PyObject *py_string_network_path = Py_BuildValue("s", network_path);
    // py string "from_json"
    PyObject *py_string_from_json = Py_BuildValue("s", "from_json");
    // net = pp.from_json("path_to_network/network.json")
    net = PyObject_CallMethodObjArgs(pandapower, py_string_from_json, py_string_network_path, NULL);
    if(net==NULL)
        printf("Error while loading pandapower network.\n");
    // pp.pf
    PyObject *pp_pf = PyObject_GetAttrString(pandapower, "pf");
    // pp.pf.runpp_3ph
    pp_pf_runpp_3ph = PyObject_GetAttrString(pp_pf, "runpp_3ph");
    // py string "runpp_3ph"
    py_string_runpp_3ph = Py_BuildValue("s", "runpp_3ph");
    char model_path[1024];
    strcpy(model_path, fmuResourceLocation);
    strcat(model_path, "/../modelDescription.xml");
    updatePandapower();
    initValueReferenceArrays(model_path);
    Py_DECREF(py_string_network_path);
    Py_DECREF(py_string_from_json);
    Py_DECREF(pp_pf);
    return net;
}

void fmi2FreeInstance(fmi2Component c){
}

fmi2Status fmi2SetupExperiment(fmi2Component c, fmi2Boolean toleranceDefined, fmi2Real tolerance, fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime){
    sim_time = startTime;
    return fmi2OK;
}

fmi2Status fmi2EnterInitializationMode(fmi2Component c){
    // NOT IMPLEMENTED
    return fmi2OK;
}

fmi2Status fmi2ExitInitializationMode(fmi2Component c){
    // NOT IMPLEMENTED
    return fmi2OK;
}

fmi2Status fmi2Terminate(fmi2Component c){
    return fmi2OK;
}

fmi2Status fmi2Reset(fmi2Component c){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[]){
  PyObject *py_value = getValue(vr[0]);
  value[0] = PyFloat_AsDouble(py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[]){
  PyObject *py_value = getValue(vr[0]);
  value[0] = (int) PyLong_AsLong(py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[]){
  PyObject *py_value = getValue(vr[0]);
  value[0] = (int) PyLong_AsLong(py_value) ? fmi2True : fmi2False;
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2GetString (fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String value[]){
  PyObject *py_value = getValue(vr[0]);
  value[0] = PyUnicode_AsUTF8(py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

/* Build a python command to Set a Real in the pandapower network
*
* Search in a dataframe "dataframe" of the network for a variable with name "varName" (a row of the dataframe)
* and set the value of column "col" to the desired value
*
* note: it assumes that loads are in the same order in the network.json and modelDescription.xml
*/
 fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[]){
   PyObject *py_value = Py_BuildValue("d", value[0]);
   setValue(vr[0], py_value);
   Py_DECREF(py_value);
   return fmi2OK;
 }

fmi2Status fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[]){
  PyObject * py_value = Py_BuildValue("i", value[0]);
  setValue(vr[0], py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[]){
  PyObject * py_value = Py_BuildValue("p", value[0]);
  setValue(vr[0], py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2SetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2String value[]){
  PyObject * py_value = Py_BuildValue("s", value[0]);
  setValue(vr[0], py_value);
  Py_DECREF(py_value);
  return fmi2OK;
}

fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t *size){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate, fmi2Byte serializedState[], size_t size){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2DeSerializeFMUstate(fmi2Component c, const fmi2Byte serializedState[], size_t size, fmi2FMUstate* FMUstate){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetDirectionalDerivative(fmi2Component c, const fmi2ValueReference vUnknown_ref[], size_t nUnknown,
    const fmi2ValueReference vKnown_ref[], size_t nknown,
    const fmi2Real dvKnown[], fmi2Real dvUnknown[]){
        // NOT IMPLEMENTED
        return fmi2Error;
    }

/***************************************************
Functions for FMI2 for Co-Simulation
****************************************************/

fmi2Status fmi2SetRealInputDerivatives(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], const fmi2Real value[]){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer order[], fmi2Real value[]){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2DoStep(fmi2Component c, fmi2Real currentCommunicationPoint, fmi2Real communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint){
    if (currentCommunicationPoint != sim_time){
        return fmi2Error;
    }
    else {
        sim_time = sim_time + communicationStepSize;
        return fmi2OK;
    }
}

fmi2Status fmi2CancelStep(fmi2Component c){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status* value){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real* value){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value){
    // NOT IMPLEMENTED
    return fmi2Error;
}

fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String* value){
    // NOT IMPLEMENTED
    return fmi2Error;
}
