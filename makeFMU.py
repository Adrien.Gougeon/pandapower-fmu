#!/usr/bin/python3

import argparse
import subprocess

# Parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument('-n', '--name', type=str, help="desired name for the FMU (default=\"pandapower\")", default="pandapower")
parser.add_argument('-i', '--input', type=str, help="path to the file containing inputs ports of the FMU", required=True)
parser.add_argument('-o', '--output', type=str, help="path to the file containing outputs ports of the FMU", required=True)
parser.add_argument('-nw', '--network', type=str, help="path to the json network file for the FMU", required=True)
parser.add_argument('-u', '--unbalanced', action='store_true', help="use Unbalanced/Asymmetric/Three Phase Load flow")
args = parser.parse_args()

# Copying files in resources folder
print("Copying files into resources folder.")
subprocess.run("mkdir -p resources", shell=True)
subprocess.run("cp " + args.input + " resources/input_ports.txt", shell=True)
subprocess.run("cp " + args.output + " resources/output_ports.txt", shell=True)
subprocess.run("cp " + args.network + " resources/network.json", shell=True)

# Building xml file
print("Building modelDescription.xml.")
subprocess.run("python3 sources/xmlGenerator.py -n " + args.name, shell=True)

# Zipping FMU
if args.unbalanced:
	print("Unbalanced power flow selected by providing option -u.")
	subprocess.run("mv -n binaries/linux64/pandapower_unbalanced.so binaries/linux64/" + args.name + ".so", shell=True)
else:
	print("Balanced power flow selected (use option -u for unbalanced power flow).")
	subprocess.run("mv -n binaries/linux64/pandapower.so binaries/linux64/" + args.name + ".so", shell=True)

print("Zipping FMU.")
subprocess.run("zip -qr "+ args.name + ".fmu binaries/linux64/" + args.name + ".so " + "resources modelDescription.xml", shell=True)
