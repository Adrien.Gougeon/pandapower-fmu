# pandapower-fmu

--- 

This project aims to provide a simple way to create FMUs (Functionnal Mock-up Units) of networks generated beforehand with the [pandapower](https://pandapower.readthedocs.io/en/latest/) module in python.
The FMUs can then be used in co-simulation with [tools](https://fmi-standard.org/tools/) able to import FMUs.
The FMUs generated are in accordance with the [FMI standard](https://fmi-standard.org/).

## Compiling the FMU builder

---

If it does not exist create the build directory at the root of the project:

```
$ mkdir build
```

Then move to the build folder and compile the FMU builder:

```
$ cd build
$ cmake ..
$ make
```

## Building your FMU

---

Once the FMU builder is compiled you can create your own FMU of pandapower by running the python executable at the root of the project:

```
$ python3 makeFMU.py [-h] [-n NAME] -i INPUT -o OUTPUT -nw NETWORK [-u]
```

Or simply:
```
$ ./makeFMU.py [-h] [-n NAME] -i INPUT -o OUTPUT -nw NETWORK [-u]
```

As you can see in the command, the builder as several arguments, detailled in the following section.

### Mandatory arguments

#### -i INPUT

Define the path to the file listing the input ports that will be available in your FMU.

The file must be of the following form:
```
dataframe/variable/column/type
dataframe/variable/column/type
dataframe/variable/column/type
...
```

* "dataframe" is a dataframe of the pandapower API (load, bus...).
* "variable" is the name of the variable in the dataframe, necessary to
  find the row you want to set/get, it must not contain a "/".
* "column" is the attribute to set/get in the row.
* "type" is the type of the variable in pandapower, it must be one of the following: Real Integer Boolean String.

Here is an input file example:

```
ext_grid/Grid_Connection/vm_pu/Real
load/Load/q_mvar/Real
```

#### -o OUTPUT

Define the path to the file listing the output ports that will be available in your FMU.
It is strictly similar to the form of the input file but the ports defined will only be available for reading.

#### -nw NETWORK

Define the path to the power network json file. This network must be generated using the pandapower [to_json](https://pandapower.readthedocs.io/en/v1.2.2/file_io.html#json) method.

### Optional arguments

#### -h

Print this short help:

```
usage: makeFMU.py [-h] [-n NAME] -i INPUT -o OUTPUT -nw NETWORK [-u]

optional arguments:
  -h, --help            show this help message and exit
  -n NAME, --name NAME  desired name for the FMU (default="pandapower")
  -i INPUT, --input INPUT
                        path to the file containing inputs ports of the FMU
  -o OUTPUT, --output OUTPUT
                        path to the file containing outputs ports of the FMU
  -nw NETWORK, --network NETWORK
                        path to the json network file for the FMU
  -u, --unbalanced      use Unbalanced/Asymmetric/Three Phase Load flow
```

#### -n NAME

Define the name of the generated FMU, "pandapower" by default.

#### -u

If this argument is passed then the FMU builder will consider the power network as unbalanced and use the [appropriate pandapower method](https://pandapower.readthedocs.io/en/v2.5.0/powerflow/ac_3ph.html?highlight=runpp#pandapower.pf.runpp_3ph.runpp_3ph) to solve the power network load flow.

## Examples
Some examples are available in the examples directory.

To build those examples you must install [simgrid](https://simgrid.org/) and [simgrid-FMI](https://framagit.org/simgrid/simgrid-FMI) beforehand.

To build the examples run the following commands at the root of the project:
```
$ mkdir build
$ cd build
$ cmake -DBUILD_EXAMPLES=true ..
$ make
$ cd ..
$ ./build_examples_fmus.sh
```




