#!/bin/bash

python3 makeFMU.py -i examples/minimal_example/resources/input_ports.txt -o examples/minimal_example/resources/output_ports.txt -nw examples/minimal_example/resources/network.json
mv pandapower.fmu examples/minimal_example

python3 makeFMU.py -i examples/cigre_network/resources/input_ports.txt -o examples/cigre_network/resources/output_ports.txt -nw examples/cigre_network/resources/network.json
mv pandapower.fmu examples/cigre_network
